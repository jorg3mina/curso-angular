import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { AppState } from '../app.module';
import { DestinoViaje } from '../models/destino-viaje.models';
import { Store } from '@ngrx/store';
import { VoteDownAction, VoteUpAction } from '../models/destino-viajes-state.models';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html', 
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
 @Input() destinos: DestinoViaje;
 @Input('idx') posicion: number;
 @HostBinding('attr.class') cssClass = 'col-md-4';
 @Output() onclicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.onclicked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir(){
    this.onclicked.emit(this.destinos);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destinos));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destinos));
    return false;
  }

}
