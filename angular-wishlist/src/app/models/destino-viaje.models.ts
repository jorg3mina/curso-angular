import {v4 as uuid} from 'uuid';
export class DestinoViaje {
  public selected: boolean;
   public servicios: string[];

  //  id = uuid();
  public votes = 0;
   id: any;
   constructor(public nombre: string, public imagenUrl: string) {
       this.servicios = ['pileta', 'desayuno'];
   }
   
   setSelected(s: boolean) {
     this.selected = s; 
   }
   
   isSelected() {
        return this.selected;
    }
    
    voteUp(): any {
      this.votes++;
    }

    voteDown(): any {
      this.votes--;
    }

}

